<?
if (isset($_GET['wise_noinit']) && !empty($_GET['wise_noinit'])) {
    $strNoInit = strval($_GET['wise_noinit']);
    if ($strNoInit == 'N') {
        if (isset($_SESSION['wise_noinit']))
            unset($_SESSION['wise_noinit']);
    } elseif ($strNoInit == 'Y') {
        $_SESSION['wise_noinit'] = 'Y';
    }
}
if (!(isset($_SESSION['wise_noinit']) && $_SESSION['wise_noinit'] == 'Y')) {
	$dir = scandir(__DIR__."/wise_bitrix_class/");
	foreach ($dir as $value) {
		if ($value != "." && $value != "..") {
			require_once(__DIR__."/wise_bitrix_class/".$value);
		}
	}
	$dir = scandir(__DIR__."/wise_custom_class/");
	foreach ($dir as $value) {
		if ($value != "." && $value != "..") {
			$arClasses[str_replace(".php", "",$value)]="/local/php_interface/wise_custom_class/".$value;
		}
	}
	$dir = scandir(__DIR__."/wise_events_class/");
	foreach ($dir as $value) {
		if ($value != "." && $value != "..") {
			$arClasses[str_replace(".php", "",$value)]="/local/php_interface/wise_events_class/".$value;
		}
	}
    
    if ($arClasses) {        
        CModule::AddAutoloadClasses(
            '',
            $arClasses
        );
    }
    
    
    if (file_exists(__DIR__."/ws_events.php")) {
        require_once(__DIR__."/ws_events.php");
    }

}